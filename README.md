# RETO PRIVERION
_Reto Priverion._

## Comenzando 🚀

### Pre-requisitos 📋

*  Laragon, XAMPP ...

*  php >= 7.4.19  (recomendable por ser la versión usada)

*  mysql

*  Composer

### Descarga 📥 

_Para descargar esté proyecto debes seguir los siguientes pasos:_

*  Hacer un **FORK** de esté repositorio. (UNA VEZ)

*  Copiar la url del botón clonar.

_En la carpeta /www del Laragon - WAMP ó en la /htdocs del XAMPP, abrir **Git Bash** y ejecutar:_

``` bash
git clone 'url_copiada';
```

*  Copiar la url del botón clonar en el repositorio original o del grupo.

*  Agregar vinculo a upstream **remota** en el repositorio local. (UNA VEZ)

``` bash
git remote add upstream 'url_copiada';
```

### Configuración del proyecto ⚙️

Para configurar el proyecto, lo primero es establecer la conexión a la base de datos, entonces en el archivo config/db.php y modificamos lo siguiente 


```
dsn' => 'mysql:host=localhost;dbname=reto-priverion',
```
Ahora nos ubicaremos en la capeta raíz del proyecto, abrir **Bash** y ejecutar

``` bash
composer update
```
Una vez terminado el comando anterior, se procederá a ejecutar migraciones

``` bash
yii migrate
```
Ejecutamos en el **Bash** el siguiente comando para levantar el servidor

``` bash
yii serve
```

Para ingresar al sistema, tenemos las credenciales:

```
 email: admin
 contraseña: admin
```

---
Muchas gracias. 