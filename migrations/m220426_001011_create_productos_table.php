<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%productos}}`.
 */
class m220426_001011_create_productos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%productos}}', [
            'id'     => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'marca'  => $this->string()->notNull(),
            'gramos' => $this->integer()->notNull(),
            'invima' => $this->string()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%productos}}');
    }
}
