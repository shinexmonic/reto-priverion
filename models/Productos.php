<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string $nombre
 * @property string $marca
 * @property int $gramos
 * @property string $invima
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'marca', 'gramos', 'invima'], 'required',  'message' => 'El campo {attribute} no puede estar vacío'],
            [['gramos'], 'integer',  'message' => 'El campo {attribute} debe ser entero'],
            [['nombre', 'marca', 'invima'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'marca' => 'Marca',
            'gramos' => 'Gramos',
            'invima' => 'Invima',
        ];
    }
}
